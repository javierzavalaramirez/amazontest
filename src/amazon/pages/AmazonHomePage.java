package amazon.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.common.collect.Ordering;

import amazon.pageobjects.AmazonHomePageElements;

public class AmazonHomePage {
	
	private WebDriver driver;
	public AmazonHomePageElements homePage = new AmazonHomePageElements();
	
	public AmazonHomePage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		
	}
	
	public void searchStuff(String searchText)
	{
		homePage.searchBar(driver).sendKeys(searchText);
	}
	
	public void clickSearchButton()
	{
		homePage.searchButton(driver).click();
	}
	
	public void materialRefineSearch(String filter)
	{
		if(filter.equals("plastic"))
		homePage.plasticcheckbox(driver).click();
	}
	
	public void rankRefineSearch(String min, String max)
	{
		homePage.minTextField(driver).sendKeys(min);
		homePage.maxTextField(driver).sendKeys(max);
		homePage.goByRankButton(driver).click();	
	}
	
	public void validatePriceRank()
	{
		WebElement price;
		int list = 1;
		
		for(WebElement element : homePage.firstFiveResults(driver)) {
			
			try 
			{
				price = element.findElement(By.xpath("//ul[@id='s-results-list-atf']/li["+list+"]//a//span[@class='sx-price sx-price-large']"));
			}
			catch(NoSuchElementException e) 
			{
				price = element.findElement(By.xpath("//li["+list+"]//a/span[2]"));
			}
			
			Assert.assertTrue(convertPrice(price.getText(),6) >= 20 && convertPrice(price.getText(),6) <= 100);
			list++;
		}
		
	}
	
	public Double convertPrice(String text, int substring)
	{
		Double price = 0.0;
		text = text.substring(1,substring);
		text = text.replaceAll("\\s+","");
		//text = text.replaceAll("\\.","");
		//price = Integer.parseInt(text);
		price = Double.parseDouble(text);
		return price;
	}
	
	public void sortByPrice() 
	{
		WebElement price;
		List<Double> prices = new ArrayList<>();
		int list = 1;
		
		for(WebElement element : homePage.firstFiveResults(driver)) {
			
			if(element.findElement(By.xpath("//ul[@id='s-results-list-atf']/li["+list+"]//a//span[@class='sx-price sx-price-large']")).isDisplayed())
			{
				price = element.findElement(By.xpath("//ul[@id='s-results-list-atf']/li["+list+"]//a//span[@class='sx-price sx-price-large']"));
			}
			else {
			price = element.findElement(By.xpath("//li["+list+"]//a/span[2]"));
		
			}
			prices.add(convertPrice(price.getText(), 7));
			list++;
		}
		Collections.sort(prices);
		Assert.assertTrue(Ordering.natural().isOrdered(prices));
	}
	
	public void sortByRating()
	{
		WebElement stars;
		List<Double> starsList = new ArrayList<>();
		int list = 1;
		
		for(WebElement element : homePage.firstFiveResults(driver)) {
		
			stars = element.findElement(By.xpath("//ul[@id='s-results-list-atf']/li["+list+"]//span[contains(text(),'stars')]"));
			String value = stars.getAttribute("innerHTML");
			value = value.substring(0,3);
			starsList.add(Double.parseDouble(value.replaceAll("\\s+","")));
			list++;
		}
		Collections.sort(starsList);
		Assert.assertTrue(Ordering.natural().isOrdered(starsList));
	}

}
