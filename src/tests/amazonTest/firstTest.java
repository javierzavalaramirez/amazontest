package tests.amazonTest;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import amazon.pages.AmazonHomePage;
import selenium.DriverFactory;


public class firstTest {
	
	
	@BeforeClass(alwaysRun = true)
	public void setupClass()
	{
		
	}
	
	@BeforeMethod(alwaysRun = true)
	public void setupTest()
	{

	}

	@Parameters()
	@Test(description = "Test Description")
	public void groupSetup() throws Exception{
		WebDriver driver = DriverFactory.getInstance().setDriver("chrome");
		driver.get("https://www.amazon.com");	
		
		AmazonHomePage homePage = new AmazonHomePage(driver);
		homePage.searchStuff("ipad air 2 case");
		homePage.clickSearchButton();
		homePage.materialRefineSearch("plastic");
		Thread.sleep(3000);
		homePage.rankRefineSearch("20", "100");
		homePage.validatePriceRank();
		homePage.sortByPrice();
		homePage.sortByRating();
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() 
	{
		DriverFactory.getInstance().removeDriver();
	}
}
